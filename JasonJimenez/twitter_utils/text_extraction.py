
def extract_tweet_text(input_data: dict) -> str:
    """
    From a JSON based format output, extract tweet text
    :param input_data:
        Tweet data as a dict
    :return:
        A string with the text of the tweet
    """
    return input_data.get('text')


def get_tweet_text_metrics(token_list: list, bad_lemmas: list = None) -> dict:
    """
    This function has been made for classify texts as bad text

    From token list checks if the lemma is in bad_lemmas list, if it is,
    it will be marked as 1 and added to bad tokens list, after that
    it extracts a ratio of bad tokens in the token list

    :param token_list:
    :param bad_lemmas:
    :return:
    """
    if not bad_lemmas:
        bad_lemmas = []
    output_list = []
    bad_tokens = []
    for token in token_list:
        bad_token_value = 0
        if token['lemma'] in bad_lemmas:
            bad_tokens.append(token['lemma'])
            bad_token_value = 1
        token['bad_token'] = bad_token_value
        output_list.append(token)

    return {
        'output_tokens': output_list,
        'bad_token_ratio': len(bad_tokens) / len(output_list)
    }