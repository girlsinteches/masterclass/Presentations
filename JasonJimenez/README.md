# Twitter NLP

Please check the [nlp.ipynb](nlp.ipynb) file in jupyter notebooks for getting all the info

## Before using
Ensure that you have a valid Twitter developer account and follow this instructions

[https://developer.twitter.com/en/docs/tutorials/step-by-step-guide-to-making-your-first-request-to-the-twitter-api-v2](https://developer.twitter.com/en/docs/tutorials/step-by-step-guide-to-making-your-first-request-to-the-twitter-api-v2)

Once you get yor credentials, create a config.json file with your
with your generated credentials, you will use this in code for 
performing API requests:

```json
{
  "twitter": {
    "api_key": "[API_KEY]",
    "api_secret": "[API_SECRET]",
    "bearer": "[API_BEARER_TOKEN]"
  }
}
```
## Python Requirements
```shell
pip install -r requirements.txt
```

Or for quick development
```shell
pip install spacy flask requests jupyter pandas
```

## Spacy requirements

```shell
python -m spacy download es_core_news_sm
```


# Bonus track
[rest_api.py](rest_api.py) Is a web service with spanish and english spacy initialized