# Presentations

Aquí estarán cada una de las presentaciones realizadas durante las MasterClass

*  [**Día 1**](https://gitlab.com/girlsinteches/masterclass/Presentations/-/tree/main/pdf)

✔️ [ Desarrollo de aplicaciones web y móvil con AWS Amplify - Jesús Bernal ](https://gitlab.com/girlsinteches/masterclass/Presentations/-/blob/main/pdf/girlsintech-amplify.pdf)

 👉🏻 [Repositorio de la ponencia ](https://gitlab.com/girlsinteches/masterclass/girls-in-tech-amplify)

  **Aquí puedes encontrar a Jesús** 👉🏻 <a href="https://twitter.com/bernal85">
  <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
  </a>
  </a> <a href="https://www.linkedin.com/in/jbernalvallejo/">
  <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
  </a>


  ✔️ [ La gestión y visualización de datos en Python - Jorge Calvo ](https://gitlab.com/girlsinteches/masterclass/Presentations/-/blob/main/pdf/Actividad_IncendiosAustralia.ipynb%20-%20Colaboratory.pdf)

  **Aquí puedes encontrar a Jorge** 👉🏻
  <a href="https://twitter.com/jorgemcalvo">
  <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
  </a> </a> <a href="https://www.linkedin.com/in/jorgecalvomartin/">
  <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
  </a>

  ✔️ [ ¿De qué se habla en Twitter? Clasificación automática de tweets con NLP - Jason Jimenez ](https://gitlab.com/girlsinteches/masterclass/Presentations/-/tree/main/JasonJimenez)

  **Aquí puedes encontrar a Jason** 👉🏻
  <a href="https://twitter.com/cangri2k5">
  <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
  </a> </a> <a href="https://www.linkedin.com/in/jasonjimenezcruz/">
  <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
  </a>

  ### Si quieres ver 👀  un  poco más de estás sesiones en vivo 👇

  [![MasterClass-dia1](http://img.youtube.com/vi/f7DBszRfAjQ/0.jpg)](https://youtu.be/f7DBszRfAjQ)

  > No olvides seguirnos en nuestro canal de Youtube para que estés al tanto de nuestras novedades  👉[Nuestro Canal](https://www.youtube.com/c/GirlsinTechSpain?sub_confirmation=1)👈

*  [**Día 2**](https://gitlab.com/girlsinteches/masterclass/Presentations/-/tree/main/pdf)

✔️ [ Importancia de un QA en tu equipo - Irene Duque ](https://gitlab.com/girlsinteches/masterclass/Presentations/-/blob/main/pdf/Integraci%C3%B3n%20con%20las%20redes%20sociales.pptx)

  **Aquí puedes encontrar a Irene** 👉🏻 <a href="https://twitter.com/irene_duque">
  <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
  </a> </a> <a href="https://www.linkedin.com/in/irene-duque-a9b6a7ba/">
  <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
  </a>


  ✔️ [ Análisis de datos de Twitter con Python, Gephi y t-hoarder - Atenea Vioque ](https://gitlab.com/girlsintechspain_/masterclass/Presentations/-/blob/master/pdf/)

 👉🏻 [Repositorio de la ponencia ](https://gitlab.com/girlsinteches/masterclass/como-analizar-twitter)

  **Aquí puedes encontrar a Atenea** 👉🏻 <a href="https://twitter.com/estrohacker_">
  <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >


  ✔️ [ Uso de APIS para integrar RRSS en nuestras aplicaciones - Javier Leyva ](https://gitlab.com/girlsintechspain_/masterclass/Presentations/-/blob/master/pdf/)

 👉🏻 [Repositorio de la ponencia ](https://gitlab.com/girlsinteches/masterclass/scrappingTwitter)

 **Aquí puedes encontrar a Javier** 👉🏻 <a href="https://twitter.com/javierseiglie">
    <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
    </a> </a> <a href="https://www.linkedin.com/in/javierseiglie">
    <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
    </a>

### Si quieres ver 👀  un  poco más de estás sesiones en vivo 👇

[![masterclass-dia2](http://img.youtube.com/vi/79f5YM1MJkw/0.jpg)](https://youtu.be/79f5YM1MJkw)
  > No olvides seguirnos en nuestro canal de Youtube para que estés al tanto de nuestras novedades  👉[Nuestro Canal](https://www.youtube.com/c/GirlsinTechSpain?sub_confirmation=1)👈


*  [**Día 3**](https://gitlab.com/girlsinteches/masterclass/Presentations/-/tree/main/pdf)

  ✔️ [ 10 Consejos SEO para enamorar a Google  - Viviana Otero ](https://gitlab.com/girlsinteches/masterclass/Presentations/-/blob/main/pdf/10%20Consejos%20para%20enamorar%20a%20Google-GIT.pdf)


 **Aquí puedes encontrar a Viviana** 👉🏻 <a href="https://twitter.com/VivianaOtero_">
    <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
    </a> </a> <a href="https://www.linkedin.com/in/vivianaotero/">
    <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
    </a>


  ✔️ [ Cómo usar Python para encontrar oportunidades de optimización en SEO - Lucía Sánchez ](https://gitlab.com/girlsinteches/masterclass/Presentations/-/blob/main/pdf/Python_SEO.pdf)

  **Aquí puedes encontrar a Lucía** 👉🏻 <a href="https://twitter.com/Lu_San2">
     <img src="https://img.shields.io/badge/-Twitter-1DA1F2?logo=twitter)" width="50" height="20" >
     </a> </a> <a href="https://www.linkedin.com/in/luciasanchezh/">
     <img src="https://img.shields.io/badge/-LinkedIn-blue" width="50" height="20" alt="LinkedIn">
     </a>

 👉🏻 [Repositorio de la ponencia ](https://gitlab.com/girlsinteches/masterclass/py-redirects-chain-seo)

### Si quieres ver 👀  un  poco más de estás sesiones en vivo 👇

[![masterclass-dia3](http://img.youtube.com/vi/LLt6L9lvluU/0.jpg)](https://youtu.be/LLt6L9lvluU)

  > **No olvides seguirnos en nuestro canal de Youtube para que estés al tanto de nuestras novedades  👉[Nuestro Canal](https://www.youtube.com/c/GirlsinTechSpain?sub_confirmation=1)👈 y colocar me gusta ❤️ para llegar a mas personas. ¡Gracias!**
