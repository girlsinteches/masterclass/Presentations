import spacy
import os
import json

try:
    spacy_language_dict = json.load(
        open(
            os.getenv('SPACY_LANGUAGE_DICT', './spacy_language.json'),
            mode='r',
            encoding='utf-8'  # This is for Windows users
        )
    )
except OSError:
    if os.getenv('DISPLAY_DEBUG_MESSAGES', 'false').lower() == 'true':
        print("Non JSON provided or non valid JSON format using default")
    spacy_language_dict = {
        'es': 'es_core_news_sm'
    }


def load_spacy_instance(lang: str) -> spacy.language:
    """
    Returns a loaded spacy instance based on
    the input language
    :param lang:
        Language that spacy will use
    :return spacy.language:
        Spacy instance ready for being used
    """
    try:
        return spacy.load(
            spacy_language_dict.get(lang)
        )
    except OSError:
        print(f"Language does not exists, please run the following command "
              f"for installing a new language: \n python -m spacy download {lang} ")
        return None


def tokens_to_dicts(tokens: iter, pos_filters: list = None) -> list[dict]:
    """
    Transforms token collection into list
    :param tokens:
    :param pos_filters:
    :return:
    """
    output_list = []
    if not pos_filters:
        pos_filters = []
    for token in tokens:
        if not token.is_stop and token.pos_ not in pos_filters:
            output_list.append({
                'text': token.text,
                'lemma': token.lemma_,
                'pos': token.pos_,
                'tag': token.tag_
            })
    return output_list
