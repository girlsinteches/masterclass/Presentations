import requests

class TwitterApiConnection:
    _tweet_fields: str = "tweet.fields=author_id,entities,public_metrics,source,text,context_annotations,lang"
    _max_results: str = "max_results=20"
    _twitter_paths: dict = {
        'search': 'https://api.twitter.com/2/tweets/search/recent?query={search_query}&{max_results}&{tweet_fields}',
        'single_tweet': 'https://api.twitter.com/2/tweets/{tweet_id}?{tweet_fields}',
        'twitter_user': 'https://api.twitter.com/2/users/{user_id}'
    }
    _bearer_token: str = None

    def __init__(self, bearer_token: str):
        self._bearer_token = bearer_token

    def search_tweets(self, query: str, language: str = "es") -> dict:
        return requests.get(
            url=self._twitter_paths.get('search').format(
                search_query=f"(lang: {language}){query}",
                max_results=self._max_results,
                tweet_fields=self._tweet_fields
            ),
            headers={'Authorization': f'Bearer {self._bearer_token}'}
        ).json()
